#!/bin/bash
# Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
# SPDX-License-Identifier: MIT-0

AWS_REGION='us-west-2'

aws amp create-workspace --region ${AWS_REGION} --alias demo-workspace
